import React from "react"
import { Admin, Resource } from "react-admin"
import jsonServerProvider from "ra-data-json-server"

import { UserList, UserCreate, UserEdit } from "./resources/Utilisateur"
import { SalleList, SalleEdit, SalleCreate } from "./resources/Salle"
import {
  ReservationList,
  ReservationCreate,
  ReservationEdit
} from "./resources/Reservation"

const dataProvider = jsonServerProvider("http://localhost:3000")

const App = () => (
  <Admin dataProvider={dataProvider}>
    <Resource
      name="utilisateur"
      list={UserList}
      create={UserCreate}
      edit={UserEdit}
    />
    <Resource
      name="salle"
      list={SalleList}
      edit={SalleEdit}
      create={SalleCreate}
    />
    <Resource
      name="reservation"
      list={ReservationList}
      create={ReservationCreate}
      edit={ReservationEdit}
    />
  </Admin>
)

export default App
