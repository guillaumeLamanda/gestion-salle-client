// in src/users.js
import React from "react"
import {
  List,
  Datagrid,
  TextField,
  DateField,
  ReferenceField,
  SimpleForm,
  Edit,
  Create,
  DateInput,
  ReferenceInput,
  SelectInput
} from "react-admin"

export const ReservationList = props => (
  <List {...props}>
    <Datagrid rowClick="edit">
      <TextField source="id" />
      <DateField source="debut" />
      <DateField source="fin" />
      <ReferenceField label="Salle" source="SalleId" reference="salle">
        <TextField source="nom" />
      </ReferenceField>
      <ReferenceField
        label="Utilisateur"
        source="UtilisateurId"
        reference="utilisateur"
      >
        <TextField source="nom" />
      </ReferenceField>
    </Datagrid>
  </List>
)
export const ReservationEdit = props => (
  <Edit {...props}>
    <SimpleForm>
      <DateInput source="debut" />
      <DateInput source="fin" />
      <ReferenceInput label="Salle" source="SalleId" reference="salle">
        <SelectInput optionText="nom" />
      </ReferenceInput>
      <ReferenceInput
        label="Utilisateur"
        source="UtilisateurId"
        reference="utilisateur"
      >
        <SelectInput optionText="nom" />
      </ReferenceInput>
    </SimpleForm>
  </Edit>
)
export const ReservationCreate = props => (
  <Create {...props}>
    <SimpleForm>
      <DateInput source="debut" />
      <DateInput source="fin" />
      <ReferenceInput label="Salle" source="SalleId" reference="salle">
        <SelectInput optionText="nom" />
      </ReferenceInput>
      <ReferenceInput
        label="Utilisateur"
        source="UtilisateurId"
        reference="utilisateur"
      >
        <SelectInput optionText="nom" />
      </ReferenceInput>
    </SimpleForm>
  </Create>
)
