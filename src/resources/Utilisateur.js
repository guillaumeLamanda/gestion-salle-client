// in src/users.js
import React from "react"
import {
  List,
  Datagrid,
  TextField,
  SimpleForm,
  TextInput,
  Create,
  Edit
} from "react-admin"

export const UserList = props => (
  <List {...props}>
    <Datagrid rowClick="edit">
      <TextField source="id" />
      <TextField source="nom" />
      <TextField source="prenom" />
    </Datagrid>
  </List>
)

export const UserCreate = props => (
  <Create {...props}>
    <SimpleForm>
      <TextInput source="nom" />
      <TextInput source="prenom" />
    </SimpleForm>
  </Create>
)

export const UserEdit = props => (
  <Edit {...props}>
    <SimpleForm>
      <TextInput source="nom" />
      <TextInput source="prenom" />
    </SimpleForm>
  </Edit>
)
