// in src/users.js
import React from "react"
import {
  List,
  Datagrid,
  TextField,
  TextInput,
  Create,
  Edit,
  SimpleForm
} from "react-admin"

export const SalleList = props => (
  <List {...props}>
    <Datagrid rowClick="edit">
      <TextField source="id" />
      <TextField source="nom" />
      <TextField source="type" />
    </Datagrid>
  </List>
)
export const SalleCreate = props => (
  <Create {...props}>
    <SimpleForm rowClick="edit">
      <TextInput source="nom" />
      <TextInput source="type" />
    </SimpleForm>
  </Create>
)
export const SalleEdit = props => (
  <Edit {...props}>
    <SimpleForm rowClick="edit">
      <TextInput source="nom" />
      <TextInput source="type" />
    </SimpleForm>
  </Edit>
)
